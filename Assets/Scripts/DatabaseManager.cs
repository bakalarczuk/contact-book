﻿using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;
using System.IO;
using System.Collections.Generic;

public class DatabaseManager : Singleton<DatabaseManager>
{
	public IDbConnection dbcon;
	private string connection;


	void Awake()
	{
		connection = "URI=file:" + Application.persistentDataPath + "/Contact_Book_DB";
		dbcon = new SqliteConnection(connection);
		dbcon.Open();

		IDbCommand dbcmd;
		IDataReader reader;

		dbcmd = dbcon.CreateCommand();
		string q_createTable =
			"CREATE TABLE IF NOT EXISTS contacts (" +
			"id INTEGER PRIMARY KEY," +
			 " date TEXT," +
			 " name TEXT," +
			 " lastname TEXT," +
			 " description TEXT," +
			" email TEXT," +
			" address TEXT," +
			" phone TEXT," +
			" website TEXT," +
			" facebook TEXT," +
			" messenger TEXT," +
			" whatsapp TEXT," +
			" twitter TEXT," +
			" image TEXT" +
			 ")";

		dbcmd.CommandText = q_createTable;
		reader = dbcmd.ExecuteReader();
		dbcon.Close();
	}

	public void AddData(UserData data)
	{
		dbcon = new SqliteConnection(connection);
		dbcon.Open();

		IDbCommand dbcmd;
		IDataReader reader;

		dbcmd = dbcon.CreateCommand();
		string q_insertIntoTable = "INSERT INTO contacts" +
		   " VALUES(NULL, " +
		   "'" + data.date + "'," +
		   "'" + data.name + "'," +
		   "'" + data.lastname + "'," +
		   "'" + data.description + "'," +
		   "'" + data.email + "'," +
		   "'" + data.address + "'," +
		   "'" + data.phone + "'," +
		   "'" + data.website + "'," +
		   "'" + data.facebook + "'," +
		   "'" + data.messenger + "'," +
		   "'" + data.whatsapp + "'," +
		   "'" + data.twitter + "',"+
		   "'" + data.image + "')";

		dbcmd.CommandText = q_insertIntoTable;
		reader = dbcmd.ExecuteReader();
		dbcon.Close();
	}

	public void UpdateData(UserData data)
	{
		dbcon = new SqliteConnection(connection);
		dbcon.Open();

		IDbCommand dbcmd;
		IDataReader reader;

		dbcmd = dbcon.CreateCommand();
		string q_insertIntoTable = "UPDATE contacts" +
		   " SET " +
		   "date = '" + data.date + "'," +
		   "name = '" + data.name + "'," +
		   "lastname = '" + data.lastname + "'," +
		   "description = '" + data.description + "'," +
		   "email = '" + data.email + "'," +
		   "address = '" + data.address + "'," +
		   "phone = '" + data.phone + "'," +
		   "website = '" + data.website + "'," +
		   "facebook = '" + data.facebook + "'," +
		   "messenger = '" + data.messenger + "'," +
		   "whatsapp = '" + data.whatsapp + "'," +
		   "twitter = '" + data.twitter + "'," +
		   "image = '" + data.image + "'" +
	 " WHERE id="+data.id;

		dbcmd.CommandText = q_insertIntoTable;
		reader = dbcmd.ExecuteReader();
		dbcon.Close();
	}

	public List<UserData> GetAllData()
	{
		dbcon = new SqliteConnection(connection);
		dbcon.Open();

		IDbCommand dbcmd;
		IDataReader reader;

		string q_getFromTable = "SELECT * FROM contacts";

		dbcmd = dbcon.CreateCommand();
		dbcmd.CommandText = q_getFromTable;
		reader = dbcmd.ExecuteReader();

		var output = new List<UserData>();

		while (reader.Read())
		{
			var contact = new UserData();
			contact.id = reader.GetInt32(0);
			contact.date = reader.GetString(1);
			contact.name = reader.GetString(2);
			contact.lastname = reader.GetString(3);
			contact.description = reader.GetString(4);
			contact.email = reader.GetString(5);
			contact.address = reader.GetString(6);
			contact.phone = reader.GetString(7);
			contact.website = reader.GetString(8);
			contact.facebook = reader.GetString(9);
			contact.messenger = reader.GetString(10);
			contact.whatsapp = reader.GetString(11);
			contact.twitter = reader.GetString(12);
			contact.image = reader.GetString(13);
			output.Add(contact);
		}

		dbcon.Close();

		return output;
	}

	public List<UserData> GetUserData(string condition, string searchtxt)
	{
		dbcon = new SqliteConnection(connection);
		dbcon.Open();

		IDbCommand dbcmd;
		IDataReader reader;

		string q_getFromTable = "SELECT * FROM contacts WHERE "+condition+"='"+searchtxt+"'";

		dbcmd = dbcon.CreateCommand();
		dbcmd.CommandText = q_getFromTable;
		reader = dbcmd.ExecuteReader();

		var output = new List<UserData>();

		while (reader.Read())
		{
			var contact = new UserData();
			contact.id = reader.GetInt32(0);
			contact.date = reader.GetString(1);
			contact.name = reader.GetString(2);
			contact.lastname = reader.GetString(3);
			contact.description = reader.GetString(4);
			contact.email = reader.GetString(5);
			contact.address = reader.GetString(6);
			contact.phone = reader.GetString(7);
			contact.website = reader.GetString(8);
			contact.facebook = reader.GetString(9);
			contact.messenger = reader.GetString(10);
			contact.whatsapp = reader.GetString(11);
			contact.twitter = reader.GetString(12);
			contact.image = reader.GetString(13);
			output.Add(contact);
		}

		dbcon.Close();

		return output;
	}
}
