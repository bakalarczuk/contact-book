﻿using System.Collections;
using System.Collections.Generic;
using TheNextFlow.UnityPlugins;
using UnityEngine;
using UnityEngine.UI;

public class ViewContactPanel : MonoBehaviour {

	public InputField contactName;
	public InputField contactLastName;
	public InputField contactDescription;
	public InputField contactEmail;
	public InputField contactAddress;
	public InputField contactPhone;
	public InputField contactWebsite;
	public InputField contactFacebook;
	public InputField contactMessenger;
	public InputField contactWhatsapp;
	public InputField contactTwitter;
	public RawImage contactImage;

	private AspectRatioFitter fitter;

	private void Start()
	{
		fitter = contactImage.GetComponent<AspectRatioFitter>();
	}

	public void DisplayContact(UserData contact)
	{
		ContactsManager.Instance.CurrentContact  = contact;

		contactName.Text(ContactsManager.Instance.CurrentContact.name);
		contactLastName.Text(ContactsManager.Instance.CurrentContact.lastname);
		contactDescription.Text(ContactsManager.Instance.CurrentContact.description);
		contactEmail.Text(ContactsManager.Instance.CurrentContact.email);
		contactAddress.Text(ContactsManager.Instance.CurrentContact.address);
		contactPhone.Text(ContactsManager.Instance.CurrentContact.phone);
		contactWebsite.Text(ContactsManager.Instance.CurrentContact.website);
		contactFacebook.Text(ContactsManager.Instance.CurrentContact.facebook);
		contactMessenger.Text(ContactsManager.Instance.CurrentContact.messenger);
		contactWhatsapp.Text(ContactsManager.Instance.CurrentContact.whatsapp);
		contactTwitter.Text( ContactsManager.Instance.CurrentContact.twitter);
		contactImage.SetTexture(ContactsManager.Instance.CurrentContact.LoadAvatar(fitter));
  	}

	public void UnlockFields()
	{
		contactName.Unlock();
		contactLastName.Unlock();
		contactDescription.Unlock();
		contactEmail.Unlock();
		contactAddress.Unlock();
		contactPhone.Unlock();
		contactWebsite.Unlock();
		contactFacebook.Unlock();
		contactMessenger.Unlock();
		contactWhatsapp.Unlock();
		contactTwitter.Unlock();
	}

	public void LockFields()
	{
		contactName.Lock();
		contactLastName.Lock();
		contactDescription.Lock();
		contactEmail.Lock();
		contactAddress.Lock();
		contactPhone.Lock();
		contactWebsite.Lock();
		contactFacebook.Lock();
		contactMessenger.Lock();
		contactWhatsapp.Lock();
		contactTwitter.Lock();
	}

	public void UpdateContact()
	{
		if(string.IsNullOrEmpty(contactName.text) && string.IsNullOrEmpty(contactLastName.text))
		{
			UIManager.Instance.messageBox.ShowMessage("Error", "Provide name and last name");
			return;
		}
		ContactsManager.Instance.CurrentContact.name = contactName.text;
		ContactsManager.Instance.CurrentContact.lastname = contactLastName.text;
		ContactsManager.Instance.CurrentContact.description = contactDescription.text;
		ContactsManager.Instance.CurrentContact.email = contactEmail.text;
		ContactsManager.Instance.CurrentContact.address = contactAddress.text;
		ContactsManager.Instance.CurrentContact.phone = contactPhone.text;
		ContactsManager.Instance.CurrentContact.website = contactWebsite.text;
		ContactsManager.Instance.CurrentContact.facebook = contactFacebook.text;
		ContactsManager.Instance.CurrentContact.messenger = contactMessenger.text;
		ContactsManager.Instance.CurrentContact.whatsapp = contactWhatsapp.text;
		ContactsManager.Instance.CurrentContact.twitter = contactTwitter.text;
		ContactsManager.Instance.CurrentContact.image = contactImage.StoreAvatar();

		DatabaseManager.Instance.UpdateData(ContactsManager.Instance.CurrentContact);

		UIManager.Instance.messageBox.ShowMessage("", "Your Contact: " + ContactsManager.Instance.CurrentContact.name +" " + ContactsManager.Instance.CurrentContact.lastname + " was succesfuly saved.");
	}

}
