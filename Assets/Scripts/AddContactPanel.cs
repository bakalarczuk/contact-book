﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using TheNextFlow.UnityPlugins;
using UnityEngine;
using UnityEngine.UI;

public class AddContactPanel : MonoBehaviour {

	public InputField contactName;
	public InputField contactLastName;
	public InputField contactDescription;
	public InputField contactEmail;
	public InputField contactAddress;
	public InputField contactPhone;
	public InputField contactWebsite;
	public InputField contactFacebook;
	public InputField contactMessenger;
	public InputField contactWhatsapp;
	public InputField contactTwitter;
	public RawImage contactImage;

	public void SaveContact()
	{
		try
		{
			if (string.IsNullOrEmpty(contactName.text) && string.IsNullOrEmpty(contactLastName.text))
			{
				UIManager.Instance.messageBox.ShowMessage("Error", "Provide name and last name");
				return;
			}
			UserData data = new UserData();
			data.date = System.DateTime.Now.ToShortDateString();
			data.name = contactName.text;
			data.lastname = contactLastName.text;
			data.description = contactDescription.text;
			data.email = contactEmail.text;
			data.address = contactAddress.text;
			data.phone = contactPhone.text;
			data.website = contactWebsite.text;
			data.facebook = contactFacebook.text;
			data.messenger = contactMessenger.text;
			data.whatsapp = contactWhatsapp.text;
			data.twitter = contactTwitter.text;
			data.image = contactImage.StoreAvatar();

			DatabaseManager.Instance.AddData(data);
			UIManager.Instance.messageBox.ShowMessage("", "Your Contact: " + data.name +" " + data.lastname + " was succesfuly added.");
		}
		catch (SqliteException e)
		{
			UIManager.Instance.messageBox.ShowMessage("", e.Message);
		}
	}
		  
	public void ClearContact()
	{
		contactName.Text(string.Empty);
		contactLastName.Text(string.Empty);
		contactDescription.Text(string.Empty);
		contactEmail.Text(string.Empty);
		contactAddress.Text(string.Empty);
		contactPhone.Text(string.Empty);
		contactWebsite.Text(string.Empty);
		contactFacebook.Text(string.Empty);
		contactMessenger.Text(string.Empty);
		contactWhatsapp.Text(string.Empty);
		contactTwitter.Text(string.Empty);
		contactImage.SetTexture(null);
	}

}
