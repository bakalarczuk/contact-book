﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class ExtensionMethods {

	public static void DestroyAllChildren(this Transform transform, Transform trash = null)
	{
		List<Transform> childrenList = new List<Transform>();
		foreach (Transform child in transform)
		{
			childrenList.Add(child);
		}
		transform.DetachChildren();
		if (trash != null)
		{
			foreach (Transform child in childrenList)
			{
				child.SetParent(trash);
				GameObject.Destroy(child.gameObject);
			}
		}
		else
		{
			childrenList.ForEach(child => GameObject.Destroy(child.gameObject));
		}
	}

	public static void Text(this InputField field, string txt)
	{ field.text = txt; }

	public static void Lock(this InputField field)
	{
		field.readOnly = true;
		field.targetGraphic.color = new Color32(200, 200, 200, 185);
	}

	public static void Unlock(this InputField field)
	{
		field.readOnly = false;
		field.targetGraphic.color = new Color32(255, 255, 255, 255);
	}

	public static void SetTexture(this RawImage image, Texture2D tex)
	{
		image.texture = tex;
	}

	public static Texture2D LoadAvatar(this UserData userData, AspectRatioFitter fitter)
	{
		Texture2D avatar = new Texture2D(64, 64);
		byte[] decodedBytes = Convert.FromBase64String(userData.image);
		avatar.LoadImage(decodedBytes);
		fitter.aspectRatio = (float)avatar.width / (float)avatar.height;
		return avatar;
	}

	public static string StoreAvatar(this RawImage image)
	{
		if(image.texture == null) { image.texture = ContactsManager.Instance.avatarNoImage; }
		RenderTexture renderTex = RenderTexture.GetTemporary(
				image.texture.width,
				image.texture.height,
				0,
				RenderTextureFormat.Default,
				RenderTextureReadWrite.Linear);

		Graphics.Blit(image.texture, renderTex);
		RenderTexture previous = RenderTexture.active;
		RenderTexture.active = renderTex;
		Texture2D readableText = new Texture2D(image.texture.width, image.texture.height);
		readableText.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
		readableText.Apply();
		byte[] pngData = readableText.EncodeToPNG();
		return Convert.ToBase64String(pngData);
	}
}
