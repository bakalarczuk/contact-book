﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContactItem : MonoBehaviour {

	[SerializeField]
	private Text contactNameText;
	[SerializeField]
	private Text descriptionText;
	[SerializeField]
	private Text detailsText;
	[SerializeField]
	private RawImage image;
	[SerializeField]
	private AspectRatioFitter fitter;
	private UserData userData;

	public void SetUserData(UserData data)
	{
		userData = data;
		contactNameText.text = userData.name+" "+userData.lastname;
		descriptionText.text = userData.description;
		detailsText.text = "Phone: "+userData.phone +"\nE-mail: "+userData.email;
		
		image.texture = userData.LoadAvatar(fitter);
	}

	public void ViewContact()
	{
		ContactsManager.Instance.ViewContact(userData);
	}
}
