﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TheNextFlow.UnityPlugins;
using System;
using System.Data;
using Mono.Data.Sqlite;

public class ContactsManager : Singleton<ContactsManager>
{

	[Header("Add Contact Panel")]
	public AddContactPanel addContactPanel;
	[Header("View/Edit Contact Panel")]
	public ViewContactPanel viewContactPanel;

	[Header("Neutral image")]
	public Texture2D avatarNoImage;	

	private UserData currentContact;
	public UserData CurrentContact { get { return currentContact; } set { currentContact = value; } }

	[Header("Search Toggles")]
	public Toggle nametgl;
	public Toggle phonetgl;
	public Toggle desctgl;

	[Header("Sort Toggles")]
	public Toggle namesrt;
	public Toggle lastnamesrt;
	public Toggle datesrt;

	[Header("Other Refs")]
	public RectTransform contactListContent;
	public ContactItem contactItemPrefab;

	private List<UserData> userDataList;

	private void Start()
	{
		viewContactPanel.LockFields();
		RefreshList(DatabaseManager.Instance.GetAllData());
	}

	public void RefreshWholeList()
	{
		RefreshList(DatabaseManager.Instance.GetAllData());
	}

	public void RefreshList(List<UserData> list)
	{
		userDataList = list;
		if (namesrt.isOn)
			userDataList.Sort((p1, p2) => p1.name.CompareTo(p2.name));
		else if (lastnamesrt.isOn)
			userDataList.Sort((p1, p2) => p1.lastname.CompareTo(p2.lastname));
		else if (datesrt.isOn)
			userDataList.Sort((p1, p2) => p1.date.CompareTo(p2.date));
		DisplayContacts();
	}


	private void DisplayContacts()
	{
		contactListContent.DestroyAllChildren();
		foreach (var userData in userDataList)
		{
			ContactItem listItem = Instantiate(contactItemPrefab, contactListContent, false);
			listItem.gameObject.SetActive(true);
			listItem.SetUserData(userData);
		}
	}

	public void ChangeStateofButton(Text btn)
	{
		if (btn.text == "Edit")
		{
			viewContactPanel.UnlockFields();
			btn.text = "Save";
		}
		else if (btn.text == "Save")
		{
			viewContactPanel.UpdateContact();
			viewContactPanel.LockFields();
			btn.text = "Edit";
		}
	}  

	public void ViewContact(UserData contact)
	{
		UIManager.Instance.ToggleVisible(UIManager.Instance.viewContact);
		viewContactPanel.DisplayContact(contact);
	}

	public void SearchContact(InputField srctxt)
	{
		if (nametgl.isOn)
		{
			RefreshList(DatabaseManager.Instance.GetUserData("name='" + srctxt.text + "' OR lastname", srctxt.text));
		}
		else if (phonetgl.isOn)
		{
			RefreshList(DatabaseManager.Instance.GetUserData("phone", srctxt.text));
		}
		if (desctgl.isOn)
		{
			RefreshList(DatabaseManager.Instance.GetUserData("description", srctxt.text));
		}
	}


	public void OpenEmail(InputField email)
	{
		if (string.IsNullOrEmpty(email.text)) return;
		Application.OpenURL("mailto:" + email.text);
	}


	public void OpenWebsite(InputField website)
	{
		if (string.IsNullOrEmpty(website.text)) return;
		Application.OpenURL(website.text);
	}


	public void OpenWebsiteFB(InputField fburl)
	{
		if (string.IsNullOrEmpty(fburl.text)) return;
		Application.OpenURL("https://www.facebook.com/" + fburl.text);
	}

	public void OpenMessenger(InputField msgid)
	{
		if (string.IsNullOrEmpty(msgid.text)) return;
		Application.OpenURL("https://m.me/" + msgid.text);

	}

	public void MakeCall(InputField number)
	{
		if (string.IsNullOrEmpty(number.text)) return;
		Application.OpenURL("tel://" + number.text);
	}

	public void OpenSMS(InputField mobile)
	{
		if (string.IsNullOrEmpty(mobile.text)) return;
		string URL = string.Empty;

#if UNITY_ANDROID
		URL = string.Format("sms:{0}", mobile.text);
#elif UNITY_IOS
		URL ="sms:"+mobile.text;
#endif

		Application.OpenURL(URL);

	}

	public void OpenWhatsApp(InputField mobile)
	{
		if (string.IsNullOrEmpty(mobile.text)) return;
		string URL = string.Empty;

#if UNITY_ANDROID
		URL = string.Format("whatsapp://send?phone=" + mobile.text);
#elif UNITY_IOS
        URL ="whatsapp://send?phone="+mobile.text;
#endif

		Application.OpenURL(URL);

	}

	public void OpenTwitter(InputField mobile)
	{
		if (string.IsNullOrEmpty(mobile.text)) return;
		string URL = string.Empty;

#if UNITY_ANDROID
		URL = string.Format("twitter://user?user_id=" + mobile.text);
#elif UNITY_IOS
        URL ="whatsapp://send?phone="+mobile.text;
#endif

		Application.OpenURL(URL);

	}
}
