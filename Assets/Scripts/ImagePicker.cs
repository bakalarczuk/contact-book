﻿using SFB;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ImagePicker : MonoBehaviour, IPointerClickHandler
{
	public RawImage image;
	public enum AspectMode
	{
		BasedOnHeight,
		BasedOnWidth
	}

	public AspectMode aspectMode;

	public void OnPointerClick(PointerEventData eventData)
	{
		PickImage();
	}

	public void PickImage()
	{
#if UNITY_EDITOR || UNITY_STANDALONE
		// Open file with filter
		var extensions = new[] {
			new ExtensionFilter("Image Files", "png", "jpg", "jpeg" ),
			new ExtensionFilter("All Files", "*" ),
		};
		var paths = StandaloneFileBrowser.OpenFilePanel("Select an image", "", extensions, true);
		if (paths.Length > 0)
		{
			StartCoroutine(OutputRoutine(new System.Uri(paths[0]).AbsoluteUri));
		}
#else
	NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) => {
			Debug.Log("Image path: " + path);
			if (path != null)
			{
				// Create Texture from selected image
				Texture2D texture = NativeGallery.LoadImageAtPath(path, 1024);
				if (texture == null)
				{
					Debug.Log("Couldn't load texture from " + path);
					return;
				}

				NativeGallery.ImageProperties imageProp = NativeGallery.GetImageProperties(path);

				float ratio = (aspectMode == AspectMode.BasedOnWidth ? (float)imageProp.width / (float)imageProp.height : (float)imageProp.width / (float)imageProp.height);
				GetComponent<AspectRatioFitter>().aspectRatio = ratio;

				image.texture = texture;
			}
		}, "Select a PNG image", "image/png", 1024);

		Debug.Log("Permission result: " + permission);
#endif
	}

	private IEnumerator OutputRoutine(string url)
	{
		var loader = new WWW(url);
		yield return loader;
		float ratio = (aspectMode == AspectMode.BasedOnWidth ? (float)loader.texture.width / (float)loader.texture.height : (float)loader.texture.width / (float)loader.texture.height);
		GetComponent<AspectRatioFitter>().aspectRatio = ratio;
		image.texture = loader.texture;
	}


}
