﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class UIManager : Singleton<UIManager>
{
	public Animator viewContact;
	public MessageBox messageBox;

	public void ToggleVisible(Animator anim)
	{
		if (anim.GetBool("Open"))
		{
			anim.SetBool("Open", false);
		}
		else
		{
			anim.SetBool("Open", true);
		}
	}

}