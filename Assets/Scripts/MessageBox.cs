﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageBox : Singleton<MessageBox> {

	public Text title;
	public Text message;
	public Button button;

	public void ShowMessage(string titleText, string msg)
	{
		title.text = titleText;
		message.text = msg;

		gameObject.SetActive(true);
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}
}
