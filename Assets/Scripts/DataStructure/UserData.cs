﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
					   
[System.Serializable]
public class UserData
{
	public int id;
	public string date;
	public string name;
	public string lastname;
	public string description;
	public string email;
	public string address;
	public string phone;
	public string website;
	public string facebook;
	public string messenger;
	public string whatsapp;
	public string twitter;
	public string image;
}
