# Contact Book

Contact book for mobile device

# Usage

Install it on your device, run it and add, edit or delete contacts.
You can also make calls, send text messages or view profiles directly from application

# Run instructions

## For Unity

Clone or download zipped project. Open it in Unity3D editor. Press play or build it on Android or iOS device

## For Android

Get apk file from Builds directory of this repository, install it on Android device. Run application.

## For iOS

You need to build it from project directly on iOS device.